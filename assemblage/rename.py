# Copyright (C) 2021-2023 Roderick Constance
# https://gitlab.com/JustAddRobots
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This module facilitates naming and enumeration of properties and instances.
"""

from datetime import datetime
from datetime import timezone


def get_timestamp():
    """Get ISO formatted timestamp.

    Args:
        None

    Returns:
        timestamp (str): ISO formatted timestamp.
    """
    timestamp = datetime.now(timezone.utc).astimezone().isoformat()
    return timestamp


def strip_instance_num(str_):
    """Strip the blender-generated instance number. This useful for exporting
    meshes without trailing numbers in filenames.

    Args:
        str_ (str): String from which to strip instance number.

    Returns:
        str_ (str): String without instance number.
    """
    list_ = str_.split(".")
    if len(list_) > 1 and list_[-1].isnumeric():
        str_ = ".".join(list_[0:-1])
    return str_
