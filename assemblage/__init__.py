# Copyright (C) 2021-2023 Roderick Constance
# https://gitlab.com/JustAddRobots
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name": "Assemblage",
    "description": "Tools for Mesh Export to UE",
    "author": "Roderick Constance",
    "version": (0, 3, 0),
    "blender": (2, 80, 0),
    "warning": "v0.3.0",
    "support": 'COMMUNITY',
    "category": 'Mesh'
}


if "bpy" in locals():
    import importlib
    importlib.reload(operator)  # noqa: F821
    importlib.reload(panel)  # noqa: F821
    importlib.reload(properties)  # noqa: F821
else:
    import bpy
    from assemblage import operator
    from assemblage import panel
    from assemblage import properties


classes = (
    operator.ASSEMBLAGE_OT_texture_mesh,
    operator.ASSEMBLAGE_OT_export_mesh,
    panel.ASSEMBLAGE_PT_texture_mesh,
    panel.ASSEMBLAGE_PT_export_mesh,
    properties.MyProperties,
)


def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    bpy.types.Scene.myprops = bpy.props.PointerProperty(type=properties.MyProperties)


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
    del bpy.types.Scene.myprops


if __name__ == "__main__":
    register()
