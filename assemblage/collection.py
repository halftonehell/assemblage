# Copyright (C) 2021-2023 Roderick Constance
# https://gitlab.com/JustAddRobots
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This module facilitates operations on collections.
"""

import bpy

import assemblage.mesh as asm_mesh
import assemblage.rename as asm_rename
import assemblage.texture as asm_tex
import assemblage.transform as asm_xform
import assemblage.ui as asm_ui


def create_collection(collection_name):
    """Create an empty collection.

    Args:
        collection_name (str): Name of collection.

    Returns:
        collection (bpy.types.Collection): Created collection.
    """
    collection = bpy.data.collections.new(collection_name)
    bpy.context.scene.collection.children.link(collection)
    return collection


def needs_temp_override():
    # Needs review
    temp_override = False
    version = bpy.app.version
    major = version[0]
    minor = version[1]
    if major < 3 or (major == 3 and minor < 2):
        temp_override = False
    else:
        temp_override = True
    return temp_override


def get_selected_collections(**kwargs):
    # Needs review
    prefix = kwargs.setdefault("prefix", None)
    selected_collections = None
    win = bpy.context.window
    scr = win.screen
    areas_3d = [area for area in scr.areas if area.type == 'VIEW_3D']
    regions = [region for region in areas_3d[0].regions if region.type == 'WINDOW']

    if needs_temp_override():
        area_override = bpy.context.area
        if bpy.context.area.type != 'OUTLINER':  # Find the outliner
            for area in bpy.context.screen.areas:
                if area.type == 'OUTLINER':
                    area_override = area

        with bpy.context.temp_override(area=area_override):
            selects = bpy.context.selected_ids
            selected_collections = [
                sel for sel in selects if sel.rna_type.name == 'Collection'
            ]
    else:
        override = {
            'window': win,
            'screen': scr,
            'area': areas_3d[0],
            'region': regions[0]
        }
        # TODO: Add context override for old versions
        type(override)

    if prefix:
        selected_collections = [
            sel for sel in selected_collections if sel.name.startswith(prefix)
        ]
    return selected_collections


def get_mesh_from_selected(use_all_scene_objs):
    """Get mesh objects from current selections.

    Args:
        use_all_scene_objects (bool): Allow selection of all objects from
            "Scene Collection".

    Returns:
        mesh_objs (list): Selected mesh objects.
    """
    sel_objs = bpy.context.selected_objects
    mesh_objs = []
    if len(sel_objs) > 0:
        mesh_objs = [obj for obj in sel_objs if obj.type == 'MESH']
    elif (
        bpy.context.collection.name == "Scene Collection"
        and use_all_scene_objs is False
    ):
        pass
        # logging.info("Export Scene Collection Disabled")
    else:
        mesh_objs = [
            obj for obj in bpy.context.collection.all_objects if obj.type == 'MESH'
        ]
    return mesh_objs


def clone_collection(collection, **kwargs):
    """Clone collection with internal meshes intact.

    Args:
        collection (bpy.types.Collection): Collection to clone.

    Kwargs:
        suffix (str): Suffix for cloned collection.

    Returns:
        cloned_collection (bpy.types.Collection): Cloned collection.
    """
    clone_suffix = kwargs.setdefault("suffix", "TMP")
    cloned_collection_name = f"{collection.name}_{clone_suffix}"
    cloned_collection = create_collection(cloned_collection_name)
    mesh_objs = [obj for obj in collection.all_objects if obj.type == 'MESH']
    asm_mesh.clone_meshes(mesh_objs, cloned_collection.name)
    return cloned_collection


def flatten_collection_to_mesh(collection, **kwargs):
    """Flatten collection to similarly-named joined and cleaned up mesh.

    Args:
        collection (bpy.types.Collection): Collection to flatten.

    Returns:
        cleaned_mesh (bpy.types.Object): Joined and cleaned mesh.
    """
    add_solidify = kwargs.setdefault("add_solidify", False)
    thickness = kwargs.setdefault("thickness", 0.04)
    mesh_objs = [obj for obj in collection.all_objects if obj.type == 'MESH']
    modded_objs = asm_xform.apply_mods(mesh_objs)
    baked_objs = asm_xform.bake_scale(modded_objs)
    joined_mesh = asm_mesh.join_mesh(baked_objs, collection.name)
    if add_solidify:
        joined_mesh = asm_mesh.solidify_mesh(joined_mesh, thickness=thickness)
    cleaned_mesh = asm_mesh.clean_mesh(joined_mesh)
    return cleaned_mesh


def remove_collection(collection):
    """Remove collection.

    Args:
        collection (bpy.types.Collection): Collection to remove.

    Returns:
        None
    """
    for obj in collection.objects:
        bpy.data.objects.remove(obj, do_unlink=True)
    bpy.data.collections.remove(collection)
    return None


def make_texture_mesh(collection, dest_collection_name, **kwargs):
    """Make a collection of meshes into a single mesh ready for export into UE.

    Collections are joined, cleaned, unwrapped, and textured with a
    Blender test grid. The resulting mesh is then moved to a destination collection.
    This will allow easy visual inspection of the mesh before exporting.

    Args:
        collection (bpy.types.Collection): Collection to texture.
        dest_collection_name (str): Name of destination collection for mesh.

    Kwargs:
        pivot (str): Mesh pivot point.
        add_solidify (bool): Add solidify modifier.
        thickness (float): Solidify modifier thickness.
        unwrap_method (str): UV unwrap method.
        uv_margin (float): UV island margin.
        add_lightmap (bool): Add lightmap.
        lightmap_size (int): Lightmap size.

    Returns:
        mesh (bpy.types.Object): Textured mesh.
    """
    pivot = kwargs.setdefault("pivot", "bbox")
    add_solidify = kwargs.setdefault("add_solidify", False)
    thickness = kwargs.setdefault("thickness", 0.04)
    unwrap_method = kwargs.setdefault("unwrap_method", 'ANGLE_BASED')
    uv_margin = kwargs.setdefault("uv_margin", 0.02)
    add_lightmap = kwargs.setdefault("add_lightmap", False)
    lightmap_size = kwargs.setdefault("lightmap_size", 256)

    cloned_collection = clone_collection(collection)
    mesh = flatten_collection_to_mesh(
        cloned_collection,
        add_solidify=add_solidify,
        thickness=thickness,
    )

    # Operate on new merged mesh
    asm_mesh.set_pivot(mesh, pivot)
    asm_mesh.move_mesh_to_collection(mesh, dest_collection_name)
    remove_collection(cloned_collection)
    if bpy.context.mode == 'EDIT_MESH':
        bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    mesh.select_set(True)
    asm_mesh.remove_uv_maps(mesh)
    asm_mesh.make_uv_map(mesh, unwrap_method=unwrap_method, uv_margin=uv_margin)
    if add_lightmap:
        asm_mesh.make_uv_map(mesh, unwrap_method=unwrap_method, lightmap_size=lightmap_size)
    mesh = asm_mesh.clean_mesh(mesh)  # unwrap adds vertices
    image = asm_tex.create_test_grid()
    asm_ui.show_image_in_uv_editor(image)
    material = asm_tex.assign_material(
        mesh,
        material=f"{asm_rename.strip_instance_num(mesh.name)}"
    )
    asm_tex.add_texture_to_material(image, material)
    return mesh
