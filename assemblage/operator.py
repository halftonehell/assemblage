# Copyright (C) 2021-2023 Roderick Constance
# https://gitlab.com/JustAddRobots
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This module defines add-on operators
"""

import bpy

import assemblage.collection as asm_clxn
import assemblage.mesh as asm_mesh
import assemblage.rename as asm_rename
import assemblage.ui as asm_ui


class ASSEMBLAGE_OT_texture_mesh(bpy.types.Operator):
    """Texture Mesh"""
    bl_label = "Texture Mesh"
    bl_idname = "assemblage.texture_mesh"

    def execute(self, context):
        scene = context.scene
        my_props = scene.myprops
        dest_collection_name = my_props.dest_collection
        if my_props.existing == "timestamp":
            dest_collection_name = "-".join(
                [dest_collection_name, asm_rename.get_timestamp()]
            )
        asm_clxn.create_collection(dest_collection_name)
        source_collections = asm_clxn.get_selected_collections(prefix=my_props.prefix)
        bpy.context.window.workspace = bpy.data.workspaces['UV Editing']
        bpy.context.space_data.shading.type = 'MATERIAL'
        for i, collection in enumerate(source_collections):
            asm_ui.update_progress("Texturing Meshes", i / 100.0)
            asm_clxn.make_texture_mesh(
                collection,
                dest_collection_name,
                pivot=my_props.pivot,
                add_solidify=my_props.add_solidify,
                thickness=my_props.thickness,
                unwrap_method=my_props.unwrap_method,
                uv_margin=my_props.uv_margin,
                add_lightmap=my_props.add_lightmap,
                lightmap_size=my_props.lightmap_size,
            )
        self.report({'INFO'}, "Texture Complete")
        return {'FINISHED'}


class ASSEMBLAGE_OT_export_mesh(bpy.types.Operator):
    """Export Mesh"""
    bl_label = "Export Mesh"
    bl_idname = "assemblage.export_mesh"

    def execute(self, context):
        scene = context.scene
        my_props = scene.myprops
        mesh_objs = asm_clxn.get_mesh_from_selected(my_props.export_all_scene_objs)
        if len(mesh_objs) > 0:
            collection_export = asm_clxn.create_collection(
                f"EXPORT-{asm_rename.get_timestamp()}"
            )
            for i, obj in enumerate(mesh_objs):
                asm_ui.update_progress("Exporting Meshes", i / 100.0)
                clones = asm_mesh.clone_meshes(
                    [obj],
                    collection_export.name,
                    suffix="EXPORT",
                )
                obj_export = clones[0]
                if bpy.context.mode == 'EDIT_MESH':
                    bpy.ops.object.mode_set(mode='OBJECT')
                bpy.ops.object.select_all(action='DESELECT')
                bpy.context.view_layer.objects.active = obj_export
                obj_export.select_set(True)
                asm_mesh.set_normals(obj_export, my_props.normals)
                asm_mesh.set_pivot(obj_export, my_props.pivot)
                asm_mesh.snap_to_origin(obj_export)
                asm_mesh.export_fbx(
                    obj_export,
                    my_props.filepath,
                    axis_forward=my_props.axis_forward,
                    axis_up=my_props.axis_up,
                    strip_instnum=my_props.strip_instnum,
                    rotate_x_fwd=my_props.rotate_x_fwd,
                    apply_transforms=my_props.apply_transforms,
                    apply_mods=my_props.apply_mods,
                    export_armature=my_props.export_armature,
                    bake_anim=my_props.bake_anim,
                    overwrite_file=my_props.overwrite_file,
                )
                bpy.ops.object.select_all(action='DESELECT')
            asm_clxn.remove_collection(collection_export)
            self.report({'INFO'}, "Export Complete")
        return {'FINISHED'}
