# Copyright (C) 2021-2023 Roderick Constance
# https://gitlab.com/JustAddRobots
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This module facilitates operations on meshes.
"""

import bpy
import math
import numpy as np
import os
import stat
from mathutils import Matrix
from mathutils import Quaternion
from mathutils import Vector

import assemblage.rename as asm_rename
from assemblage.constants import _const as CONSTANTS


def clone_meshes(mesh_objs, collection_name, **kwargs):
    """Clone static meshes.

    Args:
        mesh_objs (list): Meshes to clone.
        collection_name (str): Collection into which meshes will be moved.

    Kwargs:
        suffix (str): Suffix for cloned mesh.

    Returns:
        cloned_meshes (list): Cloned meshes.
    """
    clone_suffix = kwargs.setdefault("suffix", "TMP")
    cloned_meshes = []
    for obj in mesh_objs:
        clone = obj.copy()
        clone.data = clone.data.copy()
        clone.name = f"{obj.name}_{clone_suffix}"
#         if clone.parent.type == 'ARMATURE':
#             clone_skel = clone.parent.copy()
#             clone_skel.data = clone_skel.data.copy()
#             clone_skel.name = f"{clone.parent.name}_{clone_suffix}"
#             bpy.context.scene.collection.objects.link(clone_skel)
#             clone.parent = clone_skel
        cloned_meshes.append(clone)
        bpy.data.collections[collection_name].objects.link(clone)
    return cloned_meshes


def join_mesh(mesh_objs, joined_name):
    """Join static meshes.

    Args:
        mesh_objs (list): Mesh objects.
        joined_name (str): Name for joined meshes.

    Returns:
        joined_obj (bpy.types.Object): Joined mesh.
    """
    if bpy.context.mode == 'EDIT_MESH':
        bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    for obj in mesh_objs:
        obj.select_set(True)
    bpy.context.view_layer.objects.active = mesh_objs[0]
    bpy.ops.object.join()
    joined_obj = bpy.context.selected_objects[0]
    joined_obj.name = joined_name.rstrip("_TMP")
    return joined_obj


def solidify_mesh(obj, **kwargs):
    """Add solidify modifier to mesh.

    Args:
        obj (bpy.types.Object): Mesh to solidify.

    Returns:
        obj (bpy.types.Object): Solidified mesh..
    """
    thickness = kwargs.setdefault("thickness", 0.004)
    bpy.ops.object.select_all(action='DESELECT')
    obj.select_set(True)
    mod = obj.modifiers.new("Solidify", 'SOLIDIFY')
    mod.solidify_mode = 'NON_MANIFOLD'
    mod.nonmanifold_thickness_mode = 'EVEN'
    mod.use_quality_normals = True
    mod.thickness = thickness
    return obj


def clean_mesh(obj):
    """Clean up static mesh.

    Removes duplicate vertices, applies transformations.

    Args:
        obj (bpy.types.Object): Mesh to clean up.

    Returns:
        obj (bpy.types.Object): Cleaned mesh.
    """
    if bpy.context.mode == 'EDIT_MESH':
        bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = obj
    obj.select_set(True)
    bpy.ops.object.transform_apply()
    if bpy.context.mode == 'OBJECT':
        bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.remove_doubles()
    return obj


def move_mesh_to_collection(obj, dest_collection_name):
    """Move static mesh into collection.

    Args:
        obj (bpy.types.Object): Mesh to move.
        dest_collection_name (str): Destination collection name.

    Returns:
        None
    """
    for collection in obj.users_collection:
        collection.objects.unlink(obj)
    bpy.data.collections[dest_collection_name].objects.link(obj)
    return None


def remove_uv_maps(mesh_obj):
    """Remove all UV maps from mesh.

    Args:
        mesh_object (bpy.types.Object): Mesh from which to remove UV maps.

    Returns:
        None
    """
    mesh_data = mesh_obj.data
    uv_maps = [uv for uv in mesh_data.uv_layers]
    while uv_maps:
        mesh_data.uv_layers.remove(uv_maps.pop())
    return None


def make_uv_map(mesh_obj, **kwargs):
    """UV unwrap mesh to map.

    Args:
        mesh_object (bpy.types.Object): Mesh to UV unwrap.

    Kwargs:
        unwrap_method (str): Unwrap method.
        lightmap_size (int): Lightmap size.
        uv_margin (float): UV island margin.

    Returns:
        None
    """
    unwrap_method = kwargs.setdefault("unwrap_method", 'ANGLE_BASED')
    bpy.context.view_layer.objects.active = mesh_obj
    if bpy.context.mode == 'OBJECT':
        bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action='SELECT')
    if "uv_margin" in kwargs.keys():
        prefix = "UV_"
        margin = kwargs["uv_margin"]
    elif "lightmap_size" in kwargs.keys():
        prefix = "LM_"
        margin = round(1.0 / kwargs["lightmap_size"] * 2, 3)
        bpy.ops.mesh.mark_seam(clear=True)
        bpy.ops.mesh.mark_seam()
    layer_name = f"{prefix}{mesh_obj.name}"
    layer = mesh_obj.data.uv_layers.get(layer_name)
    if not layer:
        layer = mesh_obj.data.uv_layers.new(name=layer_name)
    layer.active = True
    bpy.ops.uv.unwrap(
        method = unwrap_method,
        margin = margin,
        correct_aspect = True,
        fill_holes = False,
    )
    bpy.ops.mesh.mark_seam(clear=True)
    return None


def set_normals(obj, state):
    """Set direction of normals.

    Args:
        obj (bpy.types.Object): Mesh object.
        state (str): State to set direction of normals..

    Returns:
        None
    """
    if bpy.context.mode == 'EDIT_MESH':
        bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = obj
    obj.select_set(True)
    if bpy.context.mode == 'OBJECT':
        bpy.ops.object.mode_set(mode='EDIT')

    if state in 'current':
        pass
    elif state in "flip":
        bpy.ops.mesh.flip_normals()
    elif state in "outside":
        bpy.ops.mesh.normals_make_consistent(inside=False)
    elif state in "inside":
        bpy.ops.mesh.normals_make_consistent(inside=True)

    if bpy.context.mode == 'EDIT_MESH':
        bpy.ops.object.mode_set(mode='OBJECT')
    return None


def set_pivot(mesh_obj, pivot):
    """Set pivot point for mesh.

    Args:
        mesh_obj (bpy.types.Object): Mesh to set pivot.
        pivot (str): Pivot point type.

    Returns:
        None
    """
    if pivot == "bbox":  # bounding box center
        mesh_data = mesh_obj.data
        M_world = mesh_obj.matrix_world
        data = (Vector(v) for v in mesh_obj.bound_box)
        coords = np.array([Matrix() @ v for v in data])
        z = coords.T[2]
        mins = np.take(coords, np.where(z == z.min())[0], axis=0)
        v = Vector(np.mean(mins, axis=0))
        v = Matrix().inverted() @ v
        mesh_data.transform(Matrix.Translation(-v))
        M_world.translation = M_world @ v
    elif pivot == "world":  # world origin
        bpy.context.scene.cursor.location = Vector((0.0, 0.0, 0.0))
        bpy.context.scene.cursor.rotation_euler = Vector((0.0, 0.0, 0.0))
        mesh_obj.origin_set(type='ORIGIN_CURSOR')
    elif pivot == "current":
        pass
    return None


def snap_to_origin(mesh_obj):
    """Snap mesh object to origin.

    Args:
        mesh_obj (bpy.types.Object): Mesh object to snap.

    Returns:
        None
    """
    if bpy.context.mode == 'EDIT_MESH':
        bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = mesh_obj
    mesh_obj.select_set(True)
    bpy.context.scene.cursor.location = Vector((0.0, 0.0, 0.0))
    bpy.ops.view3d.snap_selected_to_cursor()
    return None


def assign_seam_to_vertex_groups(target_vg_name):
    # Needs review, unused.
    mode = bpy.context.active_object.mode
    if bpy.context.mode == 'OBJECT':
        bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_similar(type='SEAM')
    seamed_meshes = bpy.context.selected_objects
    bpy.ops.object.mode_set(mode='OBJECT')
    for mesh_obj in seamed_meshes:
        bpy.context.view_layer.objects.active = mesh_obj
        selected_edges = [e.index for e in mesh_obj.data.edges if e.select]  # noqa
        vert_groups = {}
        for vg in mesh_obj.vertex_groups:
            vert_groups[vg.name] = vg

        if target_vg_name not in vert_groups.keys():
            vert_groups[target_vg_name] = mesh_obj.vertex_groups.new(name=target_vg_name)

        bpy.ops.object.vertex_group_set_active(group=target_vg_name)
        active_index = mesh_obj.vertex_groups.active_index
        vg = mesh_obj.vertex_groups[active_index]
        # vg.add(selected_edges, 1.0, 'ADD')
    bpy.ops.object.mode_set(mode=mode)
    return None


def rotate_forward(obj, axis):
    """Rotate X-forward for export to Unreal Engine. Assumes the object already faces
    the Blender-default Y-forward (will rotate 90-degrees about Z-axis).

    Args:
        obj (bpy.typs.Object): Object to rotate.
        axis (str): Forward axis.

    Returns:
        None
    """
    angle = None
    if obj:
        if bpy.context.mode == 'EDIT_MESH':
            bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')
        bpy.context.view_layer.objects.active = obj
        obj.select_set(True)
        obj.rotation_mode = 'QUATERNION'
        if axis in ["X", "x"]:
            angle = 90.0
        elif axis in ["Y", "y"]:
            angle = -90.0

        if angle:
            quat_x_forward = Quaternion((0.0, 0.0, 1.0), math.radians(angle)).normalized()
            obj.rotation_quaternion = quat_x_forward

#         bpy.ops.transform.rotate(
#             value=90.0,
#             orient_axis='Z',
#             orient_type='GLOBAL',
#             constraint_axis=(False, False, True),
#         )
    return None


def export_fbx(mesh_obj, export_dir, **kwargs):
    """Export mesh.

    Args:
        mesh_obj (bpy.types.Object): Mesh to export.
        export_dir (str): Export directory (absolute).

    Kwargs:
        axis_forward (str): Forward axis.
        axis_up (str): Up axis.
        strip_instnum (bool): Strip Blender automatic mesh instance suffix.
        apply_transforms (bool): Apply transforms on export.
        apply_mods (bool): Apply modifier(s) on export.
        export_armature (bool): Export related armature.
        bake_anim (bool): Bake Animation(s).
        overwrite_file (bool): Overwrite existing file(s).

    Returns:
        None
    """
    axis_forward = kwargs.setdefault("axis_forward", 'Y')
    axis_up = kwargs.setdefault("axis_up", 'Z')
    strip_instnum = kwargs.setdefault("strip_instnum", True)
    apply_transforms = kwargs.setdefault("apply_transforms", True)
    apply_mods = kwargs.setdefault("apply_mods", True)
    export_armature = kwargs.setdefault("export_armature", True)
    bake_anim = kwargs.setdefault("bake_anim", False)
    overwrite_file = kwargs.setdefault("overwrite_file", True)
    rotate_x_fwd = kwargs.setdefault("rotate_x_fwd", True)

    params = {
        "add_leaf_bones": False,
        "apply_scale_options": 'FBX_SCALE_NONE',
        "apply_unit_scale": True,
        "axis_forward": axis_forward,
        "axis_up": axis_up,
        "bake_space_transform": apply_transforms,  # BROKEN in Blender API
        "batch_mode": 'OFF',
        "check_existing": False,
        "global_scale": 1.00,
        "mesh_smooth_type": 'FACE',
        "path_mode": 'AUTO',
        "use_armature_deform_only": True,
        "use_mesh_modifiers": apply_mods,
        "use_selection": True,
        "use_space_transform": False,
    }

    if bpy.context.mode == 'EDIT_MESH':
        bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = mesh_obj
    mesh_obj.select_set(True)

    obj_forward = None

    # Add armature and animation params
    if export_armature and mesh_obj.parent.type == 'ARMATURE':
        mesh_obj.parent.select_set(True)
        obj_forward = mesh_obj.parent
        prefix = CONSTANTS().SK_PREFIX
        params.update({"object_types": {'ARMATURE', 'MESH'}})
        if bake_anim:
            params.update({
                "bake_anim": True,
                "bake_anim_force_startend_keying": True,
                "bake_anim_use_all_actions": False,
                "bake_anim_use_all_bones": True,
                "bake_anim_use_nla_strips": True,
            })
        else:
            params.update({
                "bake_anim": False,
            })
    else:
        obj_forward = mesh_obj
        prefix = CONSTANTS().SM_PREFIX
        params.update({"object_types": {'MESH'}})

    if rotate_x_fwd:
        rotate_forward(obj_forward, "X")
        bpy.context.view_layer.objects.active = mesh_obj
        mesh_obj.select_set(True)

    if apply_transforms:
        bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)

    # Add export filepath param
    basename = mesh_obj.name
    basename = basename.removesuffix("_EXPORT")
    if strip_instnum:
        basename = asm_rename.strip_instance_num(basename)
    export_path = f"{export_dir}/{prefix}{basename}.fbx"
    if os.path.exists(export_path) and overwrite_file:
        filestat = os.stat(export_path)
        os.chmod(export_path, filestat.st_mode | stat.S_IWUSR)
    params.update({"filepath": export_path})

    bpy.ops.export_scene.fbx(**params)
    if rotate_x_fwd:
        rotate_forward(obj_forward, "Y")  # Revert rotation after export

    return None
