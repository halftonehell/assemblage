# Copyright (C) 2021-2023 Roderick Constance
# https://gitlab.com/JustAddRobots
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This module defines add-on properties.
"""

import bpy

from assemblage.constants import _const as CONSTANTS


class MyProperties(bpy.types.PropertyGroup):
    seam_group_name: bpy.props.StringProperty(
        name="UV Seam Group",
        default="SEAM_UV"
    )
    seam_group_overwrite: bpy.props.BoolProperty(
        name="Overwrite",
        default=True
    )
    prefix: bpy.props.StringProperty(
        name="Prefix",
        # default="MY_"
        default=CONSTANTS().CUSTOM_COLLECTIONS_PREFIX
    )
    dest_collection: bpy.props.StringProperty(
        name="Destination",
        default="ASSEMBLAGE"
    )
    existing: bpy.props.EnumProperty(
        items=[
            ("overwrite", "Overwrite", "Overwrite existing collection", '', 0),
            ("timestamp", "Timestamp", "Append timestamp to collection", '', 1)
        ],
        default="timestamp"
    )
    add_solidify: bpy.props.BoolProperty(
        name="Solidify",
        default=False
    )
    thickness: bpy.props.FloatProperty(
        name="Thickness",
        default=0.004,
        min=0,
        max=1,
        precision=3,
        step=1
    )
    uv_margin: bpy.props.FloatProperty(
        name="UV margin",
        default=0.02,
        min=0,
        max=1,
        precision=2,
        step=1
    )
    add_lightmap: bpy.props.BoolProperty(
        name="Lightmap",
        default=False
    )
    lightmap_size: bpy.props.IntProperty(
        name="Lightmap Size",
        default=128,
        min=64,
        max=1024,
    )
    unwrap_method: bpy.props.EnumProperty(
        items=[
            ('ANGLE_BASED', "Angle-based", "Angle-based", '', 0),
            ('CONFORMAL', "Conformal", "Conformal", '', 1),
        ],
        default='ANGLE_BASED'
    )
    map_all_scene_objs: bpy.props.BoolProperty(
        name="Enable All Scene Lightmap",
        default=False
    )
    filepath: bpy.props.StringProperty(
        name="Export Folder",
        subtype='DIR_PATH',
        default="/tmp"
    )
    overwrite_file: bpy.props.BoolProperty(
        name="Overwrite File",
        default=True
    )
    rotate_x_fwd: bpy.props.BoolProperty(
        name="Rotate X-forward",
        default=True
    )
    apply_transforms: bpy.props.BoolProperty(
        name="Apply Transforms",
        default=False
    )
    apply_mods: bpy.props.BoolProperty(
        name="Apply Modifiers",
        default=True
    )
    export_armature: bpy.props.BoolProperty(
        name="Export Armature",
        default=True
    )
    bake_anim: bpy.props.BoolProperty(
        name="Bake Animations",
        default=False
    )
    strip_instnum: bpy.props.BoolProperty(
        name="Strip Inst Num",
        default=True
    )
    export_all_scene_objs: bpy.props.BoolProperty(
        name="Enable All Scene Export",
        default=False
    )
    normals: bpy.props.EnumProperty(
        items=[
            ("current", "Current", "Keep Current", '', 0),
            ("flip", "Flip", "Flip", '', 1),
            ("outside", "Outside", "All Outside", '', 2),
            ("inside", "Inside", "All Inside", '', 3),
        ],
        default="current"
    )
    pivot: bpy.props.EnumProperty(
        items=[
            ("world", "World", "World Origin", '', 0),
            ("bbox", "BBox", "Bounding Box Bottom", '', 1),
            ("current", "Current", "Keep Current", '', 2),
        ],
        default="bbox"
    )
    axis_forward: bpy.props.EnumProperty(
        items=[
            ("X", "X", "X Axis", '', 0),
            ("-X", "-X", "-X Axis", '', 1),
            ("Y", "Y", "Y Axis", '', 2),
            ("-Y", "-Y", "-Y Axis", '', 3),
            ("Z", "Z", "Z Axis", '', 4),
            ("-Z", "-Z", "-Z Axis", '', 5),
        ],
        default="Y"
    )
    axis_up: bpy.props.EnumProperty(
        items=[
            ("X", "X", "X Axis", '', 0),
            ("-X", "-X", "-X Axis", '', 1),
            ("Y", "Y", "Y Axis", '', 2),
            ("-Y", "-Y", "-Y Axis", '', 3),
            ("Z", "Z", "Z Axis", '', 4),
            ("-Z", "-Z", "-Z Axis", '', 5),
        ],
        default="Z"
    )
