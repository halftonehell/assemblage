# Copyright (C) 2021-2023 Roderick Constance
# https://gitlab.com/JustAddRobots
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This module defines package constants.
"""


def constant(f):
    def fset(self, value):
        raise TypeError

    def fget(self):
        return f()
    return property(fget, fset)


class _const(object):

    @constant
    def CUSTOM_COLLECTIONS_PREFIX():
        return "MY_"

    @constant
    def SM_PREFIX():
        return "SM_"

    @constant
    def SK_PREFIX():
        return "SK_"
