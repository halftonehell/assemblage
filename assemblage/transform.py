# Copyright (C) 2021-2023 Roderick Constance
# https://gitlab.com/JustAddRobots
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This module facilitates transformations on objects.
"""

import bpy


def apply_mods(objs):
    """Apply all modifiers to object.

    Args:
        objs (list): Objects to apply mods.

    Reurns:
        modded_objs (list): Modded objects.
    """
    if bpy.context.mode == 'EDIT_MESH':
        bpy.ops.object.mode_set(mode='OBJECT')
    modded_objs = []
    for obj in objs:
        bpy.ops.object.select_all(action='DESELECT')
        bpy.context.view_layer.objects.active = obj
        obj.select_set(True)
        for mod in obj.modifiers:
            bpy.ops.object.modifier_apply(
                modifier=mod.name,
                report=True
            )
        modded_objs.append(obj)
    return modded_objs


def bake_scale(objs):
    """Apply scale.

    Args:
        objs (list): Objects to bake.

    Reurns:
        baked_objs (list): Baked objects.
    """
    if bpy.context.mode == 'EDIT_MESH':
        bpy.ops.object.mode_set(mode='OBJECT')
    baked_objs = []
    for obj in objs:
        bpy.ops.object.select_all(action='DESELECT')
        obj.select_set(True)
        bpy.ops.object.make_single_user(
            type='SELECTED_OBJECTS',
            object=True,
            obdata=True,
            material=True,
            animation=True
        )
        bpy.ops.object.transform_apply(
            location=False,
            rotation=False,
            scale=True
        )
        baked_objs.append(obj)
    return baked_objs
