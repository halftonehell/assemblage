# Copyright (C) 2021-2023 Roderick Constance
# https://gitlab.com/JustAddRobots
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This module defines add-on panels.
"""

import bpy

from assemblage.operator import ASSEMBLAGE_OT_export_mesh
from assemblage.operator import ASSEMBLAGE_OT_texture_mesh


class ASSEMBLAGE_PT_texture_mesh(bpy.types.Panel):
    """Texture Mesh Panel"""
    bl_label = "Texture Mesh"
    bl_idname = "assemblage.texture_mesh_panel"
    bl_space_type = "VIEW_3D"
    bl_region_type = 'UI'
    bl_category = "Assemblage"

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        my_props = scene.myprops

        col = layout.column(align=True)
        title_pct = 0.4

        row = col.split(factor=title_pct, align=True)
        row.label(text="Prefix")
        row.prop(my_props, 'prefix', text="")

        row = col.split(factor=title_pct, align=True)
        row.label(text="Destination")
        row.prop(my_props, 'dest_collection', text="")

        row = col.split(factor=title_pct, align=True)
        row.label(text="")
        row.prop(my_props, 'existing', text="")

        row = col.split(factor=title_pct, align=True)
        row.label(text="Unwrap Method")
        row.prop(my_props, 'unwrap_method', text="")

        row = col.split(factor=title_pct, align=True)
        row.label(text="UV Margin")
        row.prop(my_props, 'uv_margin', text="")

        row = col.split(factor=title_pct, align=True)
        row.label(text="Lightmap")
        subcol = row.split(factor=0.3, align=True)
        subcol.prop(my_props, 'add_lightmap', text="")
        subcol.prop(my_props, 'lightmap_size', text="")

        row = col.split(factor=title_pct, align=True)
        row.label(text="Solidify")
        subcol = row.split(factor=0.3, align=True)
        subcol.prop(my_props, 'add_solidify', text="")
        subcol.prop(my_props, 'thickness', text="")

        row = col.split(factor=title_pct, align=True)
        row.label(text="Pivot")
        row.prop(my_props, 'pivot', text="")

        row = col.row(align=True)
        row.operator(ASSEMBLAGE_OT_texture_mesh.bl_idname, text="Texture")


class ASSEMBLAGE_PT_export_mesh(bpy.types.Panel):
    """Export Mesh Panel"""
    bl_label = "Export Mesh"
    bl_idname = "assemblage.export_mesh_panel"
    bl_space_type = "VIEW_3D"
    bl_region_type = 'UI'
    bl_category = "Assemblage"

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        my_props = scene.myprops

        col = layout.column(align=True)
        title_pct = 0.30
        axis_pct = 0.70
        boolean_pct = 0.90

        row = col.split(factor=title_pct, align=True)
        row.label(text="Export Dir")
        row.prop(my_props, 'filepath', text="")

        row = col.split(factor=title_pct, align=True)
        row.label(text="Normals")
        row.prop(my_props, 'normals', text="")

        row = col.split(factor=title_pct, align=True)
        row.label(text="Pivot")
        row.prop(my_props, 'pivot', text="")

        row = col.split(factor=axis_pct, align=True)
        row.label(text="Axis Forward")
        row.prop(my_props, 'axis_forward', text="")

        row = col.split(factor=axis_pct, align=True)
        row.label(text="Axis Up")
        row.prop(my_props, 'axis_up', text="")

        row = col.split(factor=boolean_pct, align=True)
        row.label(text="Rotate X-forward")
        row.prop(my_props, 'rotate_x_fwd', text="")

        row = col.split(factor=boolean_pct, align=True)
        row.label(text="Apply Transforms")
        row.prop(my_props, 'apply_transforms', text="")

        row = col.split(factor=boolean_pct, align=True)
        row.label(text="Apply Modifiers")
        row.prop(my_props, 'apply_mods', text="")

        row = col.split(factor=boolean_pct, align=True)
        row.label(text="Export Armature")
        row.prop(my_props, 'export_armature', text="")

        row = col.split(factor=boolean_pct, align=True)
        row.label(text="Bake Animations")
        row.prop(my_props, 'bake_anim', text="")

        row = col.split(factor=boolean_pct, align=True)
        row.label(text="Strip Instance Number")
        row.prop(my_props, 'strip_instnum', text="")

        row = col.split(factor=boolean_pct, align=True)
        row.label(text="Enable All Scene Export")
        row.prop(my_props, 'export_all_scene_objs', text="")

        row = col.split(factor=boolean_pct, align=True)
        row.label(text="Overwrite File(s)")
        row.prop(my_props, 'overwrite_file', text="")

        row = col.row(align=True)
        row.operator(ASSEMBLAGE_OT_export_mesh.bl_idname, text="Export")
