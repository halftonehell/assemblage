# assemblage

Blender Automatic Texture and Export Add-on

<div align="center">
![Add-on Screenshot](readme/screenshot-assemblage-01.png)
</div>

## About

This Blender add-on allows for faster iteration of static mesh creation by cloning a collection of meshes into a single merged mesh. Collections are automatically joined, cleaned, UV-unwrapped, and textured by the add-on's **Texture** tool. The artist can then optimise the UV layout (if she chooses) then export the mesh file with the **Export** tool.

## Background

This Blender add-on is part of my proof-of-concept pipeline & tools workflow for automation of the Unreal Engine course [Build a Detective's Office Game Environment](https://dev.epicgames.com/community/learning/courses/WK/unreal-engine-build-a-detective-s-office-game-environment/YOr/unreal-engine-introduction-to-the-detective-s-office-course)

The UE course is great, but I felt there was too much toil for iterating on assets. This add-on is a tool to address that.

## Requirements

- [Python 3.6+](https://www.python.org)
- [Blender 2.8.0+](https://www.blender.org)

## Installation

This is a multifile add-on package intended for institutional deployment (works locally, too), use **pip** instead of _Blender Preferences_ for installation.

```
❯ python3 -m pip install git+https://gitlab.com/halftonehell/assemblage.git
❯ python3 -m pip show assemblage
```

- Copy *user_startup.py* to *BLENDER_USER_SCRIPTS/startup/* directory (e.g. ~/Library/Application Support/Blender/X.Y/scripts/startup/).
- Add add-on installation location to *BLENDER_ADDON_PATHS* environment variable.
- Restart Blender.
- Enable the add-on [by the usual method in Blender](https://docs.blender.org/manual/en/latest/editors/preferences/add-ons.html#enabling-disabling-add-ons).

To uninstall, disable the add-on in Blender then remove with pip.

```
❯ python3 -m pip uninstall assemblage
```

## In Action

### Texture

<div align="center">
![Add-on Video, Texture](https://github.com/JustAddRobots/vault/assets/59129905/057c333b-68ee-4a40-aafe-59f7663db6ad)
</div>

### Export

<div align="center">
![Add-on Video, Export](https://github.com/JustAddRobots/vault/assets/59129905/6841e8d8-c047-47cd-8c35-f3432175158c)
</div>

## Add-on Options

### Texture

- **Prefix**: Use only selected collections with specified prefix.
- **Destination**: Destination collection where resulting meshes are placed. By default a timestamp is appended to the name. Alternately, the artist can choose to overwrite the collection if it already exists.
- **UV Margin**: Margin between UV islands.
- **Lightmap**: Create lightmap UV with specified resolution.
- **Solidify**: Add solidify modifier with specified thickness.
- **Pivot**: Set pivot point for mesh (world origin | bounding box center | current).

### Export

- **Export Dir**: Directory for mesh file export.
- **Normals**: Set normals for exported mesh (outside | inside | flip | current).
- **Pivot**: Set pivot point for mesh (world origin | bounding box center | current).
- **Apply Modifiers**: Apply any modifiers before export.
- **Strip Instance Number**: Strip the Blender-applied instance number (.001, .002, etc.) from mesh file name on export.
- **Enable All Scene Export**: Enable export of all meshes in _Scene Collection_. **WARNING**: This may take some time if there are many meshes.
- **Overwrite File(s)**: Overwrite identically named static mesh files curently existing in the export directory.

## TODO

- [ ] Implement multiple collection selection
- [ ] Add UV map cloning for identical meshes
- [ ] Refactor for _bmesh_ optimisation
- [ ] Add automatic collision mesh generation

## Support

This proof-of-concept is an alpha, thus currently unsupported.

## License

Licensed under GNU GPL v3. See [LICENSE](LICENSE).
