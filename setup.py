import os
from setuptools import setup


def readme():
    with open("README.md") as f:
        return f.read()


with open(os.path.dirname(__file__) + "/VERSION") as f:
    pkgversion = f.read().strip()


setup(
    name="assemblage",
    version=pkgversion,
    description="Tools for 3D Pipelines",
    url="https://gitlab.com/halftonehell/assemblage.git",
    author="Roderick Constance",
    author_email="justaddrobots@icloud.com",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: End Users/Desktop",
        "Environment :: MacOS X",
        "Topic :: Multimedia :: Graphics :: 3D Modeling",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)"
    ],
    license="GPLv3",
    python_requires=">=3.6",
    packages=[
        "assemblage",
    ],
    zip_safe = False
)
